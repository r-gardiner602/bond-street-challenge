from flask import Flask, render_template, redirect, url_for, request, session, flash
from contextlib import closing
from users import *
from sqlalchemy.orm import sessionmaker
import os
app = Flask(__name__)

# App routes
@app.route('/')
def index():
    if not session.get('logged_in'):
        return render_template('index.html')
    else:
        return step1()
 
@app.route('/login', methods=['POST'])
def login():
    username = str(request.form['username'])
    password = str(request.form['pw'])
 
    Session = sessionmaker(bind = engine)
    s = Session()
    query = s.query(User).filter(User.username.in_([username]), User.password.in_([password]) )
    result = query.first()

    if result:
        session['logged_in'] = True
    else:
        flash('wrong password!')
    return redirect(redirect_url())

@app.route('/logout')
def logout():
    session['logged_in'] = False
    return redirect(url_for('index'))

@app.route('/step1', methods=['GET', 'POST'])
def step1():
    if request.method == 'POST' and request.form.get('step1', None) == 'step1':
        redirect(url_for('step2'))
    return render_template("step1.html")

@app.route('/step2', methods=['GET', 'POST'])
def step2():
    if request.method == 'POST' and request.form.get('step2', None) == 'step2':
        redirect(url_for('step3'))
    return render_template("step2.html")

@app.route('/step3', methods=['GET', 'POST'])
def step3():
    if request.method == 'POST' and request.form.get('step3', None) == 'step3':
        redirect(url_for('step4'))
    return render_template("step3.html")

@app.route('/step4', methods=['GET', 'POST'])
def step4():
    if request.method == 'POST' and request.form.get('step4', None) == 'step4':
        redirect(url_for('step5'))
    return render_template("step4.html")

@app.route('/step5', methods=['GET', 'POST'])
def step5():
    return render_template("step5.html")

# Redirect when logging in
def redirect_url():
    return request.args.get('next') or \
           request.referrer or \
           url_for('index')

# Configuration
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'tmp/bondst.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))

# db connection
def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

if __name__ == '__main__':
    app.debug = True
    app.run()