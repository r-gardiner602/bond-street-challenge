import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from usersTable import *
 
engine = create_engine('sqlite:///bondst.db', echo = True)
 
# create a Session
Session = sessionmaker( bind = engine)
session = Session()
 
user = User("admin","password")
session.add(user)
 
user = User("python","python")
session.add(user)
 
user = User("bondst","bondst")
session.add(user)

user = User("ricky", "gardiner")
session.add(user)
 
# commit the record the db
session.commit()
